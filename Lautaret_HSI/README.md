# __Imaging spectroscopy subset used as illustration with biodivMapR__ 

This directory contains two zip files:
- __ROCHENOIRE_HSI.zip__ corresponds to an image subset acquired The study site is located next to the [Col du Lautaret](https://en.wikipedia.org/wiki/Col_du_Lautaret) and the [Roche-Noire river](https://www.google.com/maps/place/Col+du+Lautaret/@45.0446771,6.4004334,1528m/data=!3m1!1e3!4m5!3m4!1s0x478a1c31d71c3b47:0x1025fabb7e969131!8m2!3d45.0358657!4d6.4051657!5m1!1e4), in the French Alps.
- ROCHENOIRE_PLOTS.zip__ contains a shapefile defining a set of circular plots (5 m radius) corresponding to different plant communities. Please contact [Philippe Choler](http://www.philippe-choler.com/) for additional information.

This data acquisition was initiated and supported by the European [ECOCHANGE project](https://cordis.europa.eu/project/id/36866) (GOCE-CT-2007-036866) and the Swiss National Science Foundation ([BIOASSEMBLE](https://p3.snf.ch/Project-125145), 31003A-125145).

copyrights: Philippe Choler, LTSER Lautaret-Oisans

