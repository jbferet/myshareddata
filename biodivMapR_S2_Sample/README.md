# __Sentinel-2 subset used as illustration with biodivMapR__ 

This directory contains:
- a Sentinel-2 subset used in the R package [biodivMapR](https://jbferet.github.io/biodivMapR/index.html) 
- a set of vector layers corresponding to different vegetation types found in the Sentinel-2 subset

Please check the [tutotial](https://jbferet.github.io/biodivMapR/articles/biodivMapR_1.html) for a description of how to access and use this dataset using R. 