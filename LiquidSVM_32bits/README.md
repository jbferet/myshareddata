# __32bit DLL for liquidSVM__ 

liquidSVM install may require a specific parameterization to run smoothly. 

```
install.packages("liquidSVM", repos="http://pnp.mathematik.uni-stuttgart.de/isa/steinwart/software/R", INSTALL_opts=c("--no-multiarch"))
```

When attempting to install the R package [prosail](https://jbferet.gitlab.io/prosail/index.html) with Windows OS, an error message may occur because the 32bit dll for liquidSVM was not installed. 

To solve this problem, this `i386` directory should be copied into the 'Path_For_My_R_distribution\library\liquidSVM\libs\' directory.
