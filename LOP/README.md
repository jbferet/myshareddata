# __Leaf optical properties datasets__ 

This directory contains a collection of datasets including leaf optical properties (directional-hemispherical reflectance and transmittance) and corresponding leaf cehmistry. 

List of the datasets available and papers to be cited when using this dataset:
- ANGERS [[1](https://doi.org/10.1016/j.rse.2008.02.012),[2](http://dx.doi.org/10.1016/j.rse.2017.03.004),[3](https://doi.org/10.1016/j.rse.2020.112173)]
- LOPEX_DRY_CAL, LOPEX_DRY_VAL, LOPEX_FRESH_CAL, LOPEX_FRESH_VAL [[3](https://doi.org/10.1016/j.rse.2020.112173),[4](http://teledetection.ipgp.jussieu.fr/opticleaf/lopex.htm)]
- LOPEX330 [[2](http://dx.doi.org/10.1016/j.rse.2017.03.004),[4](http://teledetection.ipgp.jussieu.fr/opticleaf/lopex.htm)]
- NOURAGUES [[2](http://dx.doi.org/10.1016/j.rse.2017.03.004)]

The set of leaf chemical traits available for each dataset varies depending on the dataset. 

## ANGERS dataset

The official ANGERS dataset is downloadable from http://opticleaf.ipgp.fr/index.php?page=database.
This official ANGERS dataset contains 276 samples identified in LDB_angers2003.xls

in additon to this dataset, 32 samples from Acer pseudoplatanus L. containing significant anthocyanin content, initially discarded from the dataset for the calibration of PROSPECT-5, have ben added to the current files:
- DataBioch.txt
- ReflectanceData.txt
- TransmittanceData.txt

The dataset finally contains 308 samples. For information about __estimated anthocyanin content__ reported in DataBioch.txt, please refer to [2](http://dx.doi.org/10.1016/j.rse.2017.03.004).


## LOPEX datasets

### Datasets used for the calibration and vaidation of PROSPECT-PRO

These correspond to the datasets named LOPEX_DRY_CAL, LOPEX_DRY_VAL, LOPEX_FRESH_CAL, and LOPEX_FRESH_VAL. 

The lower number of samples compared to the number of samples oriinally provided by LOPEX is explained by the elimination of samples which did not correspond to fresh broadleaves measurements. Leaf optical properties (and chemistry such as EWT and LMA) were averaged over samples from the same species, used to obtain individual measurements of biochemical constituents of interest in this experiment, including lignin, proteins (nitrogen), cellulose and starch.

Please refer to [[4](http://teledetection.ipgp.jussieu.fr/opticleaf/lopex.htm)] for a description of the original full LOPEX dataset, and to [[3](https://doi.org/10.1016/j.rse.2020.112173)] for exhaustive description of how these datasets were produced from the original LOPEX dataset.

### Full LOPEX Dataset

This full LOPEX dataset contains 330 samples extracted from the original dataset, and corresponding leaf chemistry. 

### NOURAGUES dataset

This dataset was collected during a field campaign in 2016 at the [Nouragues reseach station](https://www.leeisa.cnrs.fr/plateformes/station-scientifique-des-nouragues/), French Guiana.

EWT and LMA are the two only leaf traits available from this dataset. 

The dataset also includes directional-hemispherical reflectance and transmittance.

This dataset collection was funded by the TOSCA program grant of the French Space Agency (CNES) (HyperTropik project) and has benefitted from
"Investissement d’Avenir" grants managed by Agence Nationale de la Recherche (CEBA: ANR-10-LABX-25-01; ANAEE-France: ANR-11-INBS-0001).

## References


[1] Féret J-B, François C, Asner GP, Gitelson AA, Martin RE, Bidel LPR, Ustin SL, le Maire G & Jacquemoud S (2008). PROSPECT-4 and 5: Advances in the leaf optical properties model separating photosynthetic pigments. Remote Sensing of Environment 112(6),3030-3043. [https://doi.org/10.1016/j.rse.2008.02.012](https://doi.org/10.1016/j.rse.2008.02.012)

[2] Féret J-B, Gitelson AA, Noble SD & Jacquemoud S (2017). PROSPECT-D: Towards modeling leaf optical properties through a complete lifecycle. Remote Sensing of Environment. 193, 204–215. [http://dx.doi.org/10.1016/j.rse.2017.03.004](http://dx.doi.org/10.1016/j.rse.2017.03.004)

[3] Féret J-B, Berger K, de Boissieu F & Malenovský Z (2021). PROSPECT-PRO for estimating content of nitrogen-containing leaf proteins and other carbon-based constituents. Remote Sensing of Environment. 252, 112173. [https://doi.org/10.1016/j.rse.2020.112173](https://doi.org/10.1016/j.rse.2020.112173)

[4] Hosgood B, Jacquemound S, Andreeoli G, Verdebout J, Pedrini A & Schmuck G (1993). Leaf Optical Properties Experiment Database (LOPEX93). [access through opticleaf](http://teledetection.ipgp.jussieu.fr/opticleaf/lopex.htm)



